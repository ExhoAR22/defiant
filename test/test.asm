section .text
global _start

; This is a test program to test the example module.
; The example module registers a system call that returns rdi + rsi with rax = 500.
; This program exits with the result of the syscall as the exit code.
; The exit code can be viewed via running 'echo $?' in the shell after test exits.

_start:
mov rax, 500 ; add.ko registers the syscall for rax = 500.
mov rdi, 5 ; Parameter 1 is 5.
mov rsi, 10 ; Parameter 2 is 10.
syscall ; Make the syscall. now rax should have the value 15.
mov rdi, rax ; Make rax's value the exit code.
mov rax, 60 ; rax = 60 means sys_exit.
syscall