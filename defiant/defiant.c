#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/kallsyms.h>
#define REGISTERED_MAX 100

/*
Defiant is a kernel module the purpose of which is to allow other kernel modules add their own system
calls to the kernel in real-time, without recompiling the kernel. It does this by patching sys_ni_syscall
(a function that's called when an invalid system call is made) in memory.

After the patching, the function jumps to a custom function called fake_sys_ni_syscall, which accepts the
same parameters, has the same return type and uses the same calling convention as the real function,
but is more sophisticated. the original function would simply return -ENOSYS, while this one will check
if the requested system call is registered (by checking if it's rax value matches any of the registered
ones), and if so it will call it. If a match isn't found, the function would return -ENOSYS just like
the original one.

It's worth noting that all the system call functions accept the same parameters, have the same return
type and use the same calling convention:
asmlinkage long sys_<NAME>(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t r10, uint64_t r8, uint64_t r9)

Defiant can support up to REGISTERED_MAX custom system calls, and a single kernel module can register
multiple system calls.
*/

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ExhoAR22");
MODULE_DESCRIPTION("A Linux kernel module that enables the addition of custom system calls via kernel modules.");
MODULE_VERSION("1.0-beta");

typedef asmlinkage long (*syscall_t)(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t r10, uint64_t r8, uint64_t r9); // For ease of life.
asmlinkage long (*real_sys_ni_syscall)(void); // This will hold the pointer to the function sys_ni_syscall, which is called when an invalid syscall is made.
uint8_t patch[] = { 0x48, 0xB8, 0x90, 0x78, 0x56, 0x34, 0x12, 0xEF, 0xCD, 0xAB, 0xFF, 0xE0 }; // mov rax, 0xabcdef1234567890; jmp rax
uint8_t backup[12] = { 0 }; // Will hold the original 12 bytes from the sys_ni_syscall which'll be replaced by the patch.
syscall_t registered[REGISTERED_MAX] = { 0 }; // This array will hold pointers to custom system calls.
uint64_t registered_numbers[REGISTERED_MAX] = { 0 }, registered_count = 0; // The array holds the rax numbers of custom syscalls, the integer holds the amount of registered system calls.

// Finds a reigstered system call using it's rax number.
syscall_t find(uint64_t rax) {
    uint64_t i = 0;
    for (i; i < REGISTERED_MAX; i++) {
        if (registered_numbers[i] == rax && registered[i]) {
            return registered[i];
        }
    }
    return 0;
}

// Finds the index of the given rax value in the registered rax array (or returns -1 if nothing is found).
int find_i(uint64_t rax) {
    uint64_t i = 0;
    for (i; i < REGISTERED_MAX; i++) {
        if (registered_numbers[i] == rax && registered[i]) {
            return i;
        }
    }
    return -1;
}

// This is our custom function which the real one will jump to. It finds rax on the stack (It's there).
// And checks if it corresponds to a registered system call. If so, call it and pass on the return value.
// If not, play by the rules and return -ENOSYS.
asmlinkage long fake_sys_ni_syscall(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t r10, uint64_t r8, uint64_t r9) {
    uint64_t* stk = &rdi; 
    uint64_t rax = stk[27]; // Found this out using a loop that scanned the stack for a known value of rax.
    syscall_t f;
    printk(KERN_INFO "defiant: fake_sys_ni_syscall invoked!\n"); // Do some logging.
    printk(KERN_INFO "defiant: rax = 0x%lx\n", rax);
    printk(KERN_INFO "defiant: rdi = 0x%lx\n", rdi);
    printk(KERN_INFO "defiant: rsi = 0x%lx\n", rsi);
    printk(KERN_INFO "defiant: rdx = 0x%lx\n", rdx);
    printk(KERN_INFO "defiant: r10 = 0x%lx\n", r10);
    printk(KERN_INFO "defiant: r8 = 0x%lx\n", r8);
    printk(KERN_INFO "defiant: r9 = 0x%lx\n", r9);
    if (f = find(rax)) {
        printk(KERN_INFO "defiant: Found syscall for rax = 0x%lx. Calling...\n", rax);
        return f(rdi, rsi, rdx, r10, r8, r9);
    }
    printk(KERN_INFO "defiant: No registered syscall for rax = 0x%lx.\n", rax);
    return -ENOSYS;
}

// This function makes the page address belongs to read/write-able.
// Necssary for patching sys_ni_syscall.
void make_rw_page(unsigned long address) {
    unsigned int level;
    pte_t* pte = lookup_address(address, &level);
    if (pte->pte & ~_PAGE_RW) {
        pte->pte |= _PAGE_RW;
    }
}

// This function makes the page address belongs to readonly.
// Will be used to make sys_ni_syscall readonly again.
void make_ro_page(unsigned long address) {
    unsigned int level;
    pte_t* pte = lookup_address(address, &level);
    pte->pte &= ~_PAGE_RW;
}

// This function is called when the module is loaded.
static int __init defiant_init(void) {
    printk(KERN_INFO "defiant: Loaded.\n");
    real_sys_ni_syscall = kallsyms_lookup_name("sys_ni_syscall"); // Get a pointer to sys_ni_syscall.
    if (real_sys_ni_syscall() == -ENOSYS) { // Check it is really the right function. sys_ni_syscall simply returns -ENOSYS.
        printk(KERN_INFO "defiant: Sanity check OK.\n");
    }
    else {
        printk(KERN_ERR "defiant: Sanity check failed, probably grabbed the wrong symbol.");
        return -EFAULT;
    }
    printk(KERN_INFO "defiant: Hooking sys_ni_syscall...\n");
    *((uint64_t*)(patch + 2)) = fake_sys_ni_syscall; // Replace 0xabcdef1234567890 with the address of fake_sys_ni_syscall.
    memcpy(backup, real_sys_ni_syscall, 12); // Backup 12 first bytes from the real function.
    make_rw_page(real_sys_ni_syscall); // Make the function's bytes writeable.
    memcpy(real_sys_ni_syscall, patch, 12); // Copy our arbitrary code into sys_ni_syscall. Now it's gonna jump to fake_sys_ni_syscall.
    make_ro_page(real_sys_ni_syscall); // And make the function's bytes readonly again.
    printk(KERN_INFO "defiant: sys_ni_syscall hooked!\n");
    return 0;
}

// This function is called when the module is unloaded.
static void __exit defiant_exit(void) {
    printk(KERN_INFO "defiant: Restoring sys_ni_syscall...\n"); // Since we're being unloaded we need to remove the hook.
    make_rw_page(real_sys_ni_syscall);
    memcpy(real_sys_ni_syscall, backup, 12); // memcpy the original 12 bytes.
    make_ro_page(real_sys_ni_syscall);
    printk(KERN_INFO "defiant: sys_ni_syscall restored.\n");
    printk(KERN_INFO "defiant: Unloaded.\n");
}

// This function will be called by other modules to register a system call.
// Returns 1 if everything went ok, 0 if not.
static int defiant_register_syscall(uint64_t rax, syscall_t func) {
    printk(KERN_INFO "defiant: Trying to register a system call (@ 0x%lx) for rax = 0x%lx...\n", func, rax);
    if (registered_count >= REGISTERED_MAX || find(rax)) { // Do we have any room? is this rax taken?
        printk(KERN_INFO "defiant: Unable to register a system call for rax = 0x%lx, this rax value is already taken.\n", func, rax);
        return 0;
    }
    else { // We do? good. Save the pointer and the rax in the arrays.
        registered[registered_count] = func;
        registered_numbers[registered_count++] = rax; // Oh, and increase the amount of registered syscalls.
        printk(KERN_INFO "defiant: Registered system call for rax = 0x%lx.\n", rax);
        return 1;
    }
}

// This function will be called by other modules to unregister a system call.
// Returns 1 if everything went ok, 0 if not.
static int defiant_unregister_syscall(uint64_t rax) {
    printk(KERN_INFO "defiant: Trying to unregister a system cal registered to rax = 0x%lx...\n", rax);
    int i = find_i(rax);
    if (i < 0) { // i == -1
        printk(KERN_INFO "defiant: No such system call.\n");
        return 0;
    }
    else {
        registered[i] = 0;
        registered_numbers[i] = 0; // 0 is actually a valid place holder, because sys_ni_syscall isn't invoked for valid system calls, and on x86_64, rax 0 means read.
        printk(KERN_INFO "defiant: Unregistered system call with rax = 0x%lx.\n", rax);
        return 1;
    }
}

// These are some special macros defined in the linux headers.
module_init(defiant_init);
module_exit(defiant_exit);
EXPORT_SYMBOL(defiant_register_syscall);
EXPORT_SYMBOL(defiant_unregister_syscall);
