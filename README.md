# Defiant
A Linux kernel module that enables the addition of custom system calls via kernel modules.

## Usage:
* git clone and make the project. You are going to need the linux headers package on your system.
* Insert the module:
```
# insmod defiant/defiant.ko
```
* Verify the module is up and running by viewing some logs:
```
$ dmesg | grep defiant
```
You should see something like this if everything is OK:
```
[...] defiant: Loaded.
[...] defiant: Sanity check OK.
[...] defiant: Hooking sys_ni_syscall...
[...] defiant: sys_ni_syscall hooked!
```
* Insert some modules which implement system calls **or** write some yourself (see [example/add.c](example/add.c) for a template) and enjoy.
You could also insert the included example module and run the included test executable.
```
# insmod example/add.ko
$ test/test
$ echo $?
```
* Defiant can be removed from the kernel using rmmod:
```
# rmmod defiant
```
Modules implementing system calls using Defiant must be removed before Defiant is:
```
# rmmod add
# rmmod defiant
```

### Tested on:
* Ubuntu 18.04.1 LTS (amd64, with kernels 4.15.0-29-generic and 4.15.0-33-generic)

### Notes
* Only supports x86_64 (kernels and executables) at the moment. 32-bit executables are unable to use custom system calls as of now.
* This project must be recompiled for every different kernel version you wish to use it on.

### TODO:
* Add 32-bit executable support (?).
