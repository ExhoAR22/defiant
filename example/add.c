#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
 
MODULE_LICENSE("GPL");
MODULE_AUTHOR("ExhoAR22");
MODULE_DESCRIPTION("A Linux kernel module contatining a system call for Defiant.");
MODULE_VERSION("1.0-beta");

// Import the functions from the Defiant module.
extern int defiant_register_syscall(uint64_t rax, void* func);
extern int defiant_unregister_syscall(uint64_t rax);

// This is the custom system call.
// It accepts it's parameters in the registers rdi, rsi, rdx, r10, r8 & r9, as defined in the System-V
// x86_64 calling convention.
// This one simply adds what's in rdi and rsi and returns the result.
asmlinkage long sys_add(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t r10, uint64_t r8, uint64_t r9) {
    return rdi + rsi;
}

// This function is called when the module is loaded. Register your system call and make additional setup here.
static int __init add_init(void) {
    defiant_register_syscall(500, sys_add);
    return 0;
}

// This function is called when the module is removed. Unregister your system call here to prevent errors.
static void __exit add_exit(void) {
    defiant_unregister_syscall(500);
}

// These are some special macros defined in the linux headers.
module_init(add_init);
module_exit(add_exit); 
